package main

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"log"
	"strings"

	"code.google.com/p/go-charset/charset"
	_ "code.google.com/p/go-charset/data"
	_ "code.google.com/p/odbc"
)

var (
	//DepartamenID int
	DepartamentNAME    string
	OtdelToArray       [20]string
	DepartamentNAMEUTF string
)

func main() {
	i := 1
	// Подключаемся к базе MSSQL через ODBC драйвер
	db, err := sql.Open("odbc", "DSN=DBS0")
	if err != nil {
		fmt.Println("Error in connect DB")
		log.Fatal(err)
	}
	rows, err := db.Query("select t.DepartmentNAME from dbo.oms_Department t where t.rf_LPUID = 1078")
	//rows, err := db.Query("select username from dbo.USERS")
	if err != nil {
		fmt.Println("Error db.Query")
		log.Fatal(err)
	}
	defer rows.Close()

	//Сканируем вывод запроса по одно столбцу
	for rows.Next() {

		err := rows.Scan(&DepartamentNAME)
		if err != nil {
			log.Fatal(err)
		}
		if err != nil {
			log.Fatal(err)
		}

		// Перекодируем данные с запроса из cp1251 в UTF8
		r, err := charset.NewReader("windows-1251", strings.NewReader(DepartamentNAME))
		if err != nil {
			log.Fatal(err)
		}
		DepartamentNAME, err := ioutil.ReadAll(r)
		if err != nil {
			log.Fatal(err)
		}
		DepartamentNAMEUTF = string(DepartamentNAME)
		//		fmt.Printf("%s\n", DepartamentNAMEUTF)
		OtdelToArray[i] = DepartamentNAMEUTF
		i++
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	//fmt.Printf("%s\n", OtdelToArray)
	fmt.Printf("%s\n", OtdelToArray[2])
}
