package main

import (
	_ "code.google.com/p/odbc"
	"database/sql"
	"fmt"
	"html/template"
	"log"
	"net/http"
)

var (
	otdel     []string
	fam, name string
	ot        string
	id        int
)

func index(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "text/html")
	// Парсим html файл на параметр в форме
	t, _ := template.ParseFiles("index.html")
	r.ParseForm()
	t.Execute(w, nil)
}

func otdelen(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "text/html")
	// Записываем в speed значение с переменной в форме html
	otdel := r.PostFormValue("otdelenie")
	//fmt.Fprintln(w, otdel)
	db, err := sql.Open("odbc", "DSN=DBS0")
	if err != nil {
		fmt.Println("Error in connect DB")
		log.Fatal(err)
	}
	rows, err := db.Query("select hDED.FAMILY, hDED.Name, hDED.OT, br.rf_departmentid from dbo.stt_medicalhistory hDED inner join  dbo.v_curentmigrationpatient AS m on hDED.medicalhistoryid = m.rf_medicalhistoryid  INNER JOIN dbo.stt_stationarbranch AS br ON br.stationarbranchid = m.rf_stationarbranchid and br.rf_departmentid=?", &otdel)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Fprintf(w, "<html>\n")
	fmt.Fprintf(w, "<head><meta charset=\"windows-1251\"><title>GOLANG</title></head>\n")
	fmt.Fprintf(w, "<body>\n")
	fmt.Fprintf(w, "<table border=1>\n")
	for rows.Next() {
		if err := rows.Scan(&fam, &name, &ot, &id); err != nil {
			log.Fatal(err)
		}
		fmt.Fprint(w, "<tr><td>", fam, "&nbsp;", name, "&nbsp;", ot, "</td></tr>\n")
	}
	if err := rows.Err(); err != nil {
		log.Fatal(err)
	}
	fmt.Fprintf(w, "</table>")
	fmt.Fprintf(w, "</body>")
	fmt.Fprintf(w, "</html>")
}

func main() {
	http.HandleFunc("/", index)
	http.HandleFunc("/otdelenie", otdelen)
	http.ListenAndServe(":80", nil)
}
