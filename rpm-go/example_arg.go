package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
        "archive/tar"
        "compress/gzip"
        "io"
	"strings"
)

func handleError(_e error) {
        if _e != nil {
                log.Fatal(_e)
        }
}

func TarGzWrite(_path string, tw *tar.Writer, fi os.FileInfo) {
        fr, err := os.Open(_path)
        handleError(err)
        defer fr.Close()

        h := new(tar.Header)
        h.Name = _path
        h.Size = fi.Size()
        h.Mode = int64(fi.Mode())
        h.ModTime = fi.ModTime()

        err = tw.WriteHeader(h)
        handleError(err)

        _, err = io.Copy(tw, fr)
        handleError(err)
}

func IterDirectory(dirPath string, tw *tar.Writer) {
        dir, err := os.Open(dirPath)
        handleError(err)
        defer dir.Close()
        fis, err := dir.Readdir(0)
        handleError(err)
        for _, fi := range fis {
                curPath := dirPath + "/" + fi.Name()
                if fi.IsDir() {
                        //TarGzWrite( curPath, tw, fi )
                        IterDirectory(curPath, tw)
                } else {
                        fmt.Printf("adding... %s\n", curPath)
                        TarGzWrite(curPath, tw, fi)
                }
        }
}

func TarGz(outFilePath string, inPath string) {
        // file write
        fw, err := os.Create(outFilePath)
        handleError(err)
        defer fw.Close()

        // gzip write
        gw := gzip.NewWriter(fw)
        defer gw.Close()

        // tar write
        tw := tar.NewWriter(gw)
        defer tw.Close()

        IterDirectory(inPath, tw)

//        fmt.Println("tar.gz ok")
}

func repoAdd(dir, repo string) {
	cmd := exec.Command("urpmi.addmedia", "--distrib", "--urpmi-root", dir, repo)
	if err := cmd.Run(); err != nil {
		log.Println(err)
		fmt.Println("Ошибка создания репозитория")
	}

}

func chrootCreate(dir string, elems ...string) {
	cmd := exec.Command("urpmi", append(append([]string{},
		"--urpmi-root",
		dir,
		"--no-verify-rpm",
		"--nolock",
		"--auto",
		"--ignoresize",
		"--no-suggests",
		"basesystem-minimal"), elems...)...)
	if err := cmd.Run(); err != nil {
		log.Println(err)
		fmt.Println("Ошибка создания chroot")
	}

}

func main() {
	var (
		flArch = flag.String("arch", "", "архитектура")
		flRepo = flag.String("repo", "", "репозиторий")
		flDir  = flag.String("dir", "", "Директория для chroot")
		flName  = flag.String("name", "chroot", "Имя для chroot")
	)
	flag.Parse()

	path := os.Mkdir(*flDir, 0777)
	if path != nil {
		if err := exec.Command("rm", "-rf", *flDir).Run(); err != nil {
			log.Println(err)
			fmt.Println("Ошибка создания/удаления папки")
		}
	}

	switch *flArch {
	case "i586":
		fmt.Printf("Подключаем стандартные репозитории i586 для установки пакетов\n")
	case "x86_64":
		fmt.Printf("Подключаем стандартные репозитории x86_64 для установки пакетов\n")
	}

	//Подключаем репозитории для установки пакетов
	repoAdd(*flDir, *flRepo)
	chrootCreate(*flDir, flag.Args()...)

//        targetFilePath := *flName
        inputDirPath := "/home/saikov/"
        TarGz(*flName, strings.TrimRight(inputDirPath, "/"))

}
