package main

import (
	//"bytes"
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
)

var (
	flarch = flag.String("arch", "", "архитектура")
	flrepo = flag.String("repo", "", "репозиторий")
	fldir  = flag.String("dir", "", "Директория для chroot")
)

func repo_add() {
	cmd := exec.Command("urpmi.addmedia",
		"--distrib",
		"--urpmi-root",
		*fldir,
		*flrepo)
	if err := cmd.Run(); err != nil {
		log.Println(err)
	}

}

func chroot_create() {
	cmd := exec.Command("urpmi",
		"--urpmi-root",
		*fldir,
		"--no-verify-rpm",
		"--nolock",
		"--auto",
		"--ignoresize",
		"--no-suggests",
		"basesystem-minimal")
	if err := cmd.Run(); err != nil {
		log.Println(err)
	}

}

func main() {

	flag.Parse()
	// Создаем папку для chroot
	path := os.Mkdir(*fldir, 0777)
	if path != nil {
		cmd := exec.Command("rm", "-rf", *fldir)
		if err := cmd.Run(); err != nil {
			log.Println(err)
		}
	}
	if *flarch == "i586" {
		//	flag.PrintDefaults()
		fmt.Printf("Подключаем стандартные репозитории i586 для установки пакетов\n")
		//Подключаем репозитории для установки пакетов
		repo_add()
		chroot_create()

	}
	if *flarch == "x86_64" {
		//      flag.PrintDefaults()
		fmt.Printf("Подключаем стандартные репозитории x86_64 для установки пакетов\n")
		//Подключаем репозитории для установки пакетов
		repo_add()
		chroot_create()
	}

}

